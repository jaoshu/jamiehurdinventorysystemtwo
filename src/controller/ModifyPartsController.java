//main controller
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Jamie Hurd
 */
public class ModifyPartsController implements Initializable {
    
   @FXML
    private Button addPartSave;

    @FXML
    private Button addPartCancel;

    @FXML
    private RadioButton addPartInHouse;

    @FXML
    private RadioButton addPartOutsourced;

    @FXML
    void addPartCancel(ActionEvent event) {

    }

    @FXML
    void addPartInHouse(ActionEvent event) {

    }

    @FXML
    void addPartOutsourced(ActionEvent event) {

    }

    @FXML
    void addPartSave(ActionEvent event) {

    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
