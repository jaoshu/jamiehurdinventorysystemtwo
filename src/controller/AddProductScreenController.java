//main controller
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;

/**
 *
 * @author Jamie Hurd
 */
public class AddProductScreenController implements Initializable {
    
    @FXML
    private Button addProductSearch;

    @FXML
    private TableColumn<?, ?> addProductPartIDCol;

    @FXML
    private TableColumn<?, ?> addProductPartNameCol;

    @FXML
    private TableColumn<?, ?> addProductInventoryLevelCol;

    @FXML
    private TableColumn<?, ?> addProductPriceCol;

    @FXML
    private TableColumn<?, ?> addProductDelPartIDCol;

    @FXML
    private TableColumn<?, ?> addProductPartNameDeleteCol;

    @FXML
    private TableColumn<?, ?> addProductInventoryLevelDeleteCol;

    @FXML
    private TableColumn<?, ?> addProductPricePerUnitDelCol;

    @FXML
    private Button addProductAdd;

    @FXML
    private Button addProductDelete;

    @FXML
    private Button addProductCancel;

    @FXML
    private Button addProductSave;

    @FXML
    void AddData(ActionEvent event) {

    }

    @FXML
    void Delete(ActionEvent event) {

    }

    @FXML
    void DeleteDataInField(ActionEvent event) {

    }

    @FXML
    void ScanInputForNext(ActionEvent event) {

    }

    @FXML
    void addProductAdd(ActionEvent event) {

    }

    @FXML
    void addProductCancel(ActionEvent event) {

    }

    @FXML
    void addProductDelete(ActionEvent event) {

    }

    @FXML
    void addProductSave(ActionEvent event) {

    }

    @FXML
    void addProductSearch(ActionEvent event) {

    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
