//main controller
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;

/**
 *
 * @author Jamie Hurd
 */
public class MainScreenControler implements Initializable {
    
    @FXML
    private Button mainSearchParts;

    @FXML
    private Button mainSearchProducts;

    @FXML
    private TableColumn<?, ?> mainPartIDCol;

    @FXML
    private TableColumn<?, ?> mainPartNameCol;

    @FXML
    private TableColumn<?, ?> mainPartInvLevCol;

    @FXML
    private TableColumn<?, ?> mainPricePerUnitPartsCol;

    @FXML
    private TableColumn<?, ?> mainProductIDCol;

    @FXML
    private TableColumn<?, ?> mainProdNamCol;

    @FXML
    private TableColumn<?, ?> mainProdInvLevCol;

    @FXML
    private TableColumn<?, ?> mainPricePerUnitProdCol;

    @FXML
    private Button mainAddParts;

    @FXML
    private Button mainModifyParts;

    @FXML
    private Button mainDeleteParts;

    @FXML
    private Button mainAddProducts;

    @FXML
    private Button mainModifyProducts;

    @FXML
    private Button mainDeleteProducts;

    @FXML
    private Button mainExit;

    @FXML
    void mainAddParts(ActionEvent event) {

    }

    @FXML
    void mainAddProducts(ActionEvent event) {

    }

    @FXML
    void mainDeleteParts(ActionEvent event) {

    }

    @FXML
    void mainDeleteProducts(ActionEvent event) {

    }

    @FXML
    void mainExit(ActionEvent event) {

    }

    @FXML
    void mainModifyParts(ActionEvent event) {

    }

    @FXML
    void mainModifyProducts(ActionEvent event) {

    }

    @FXML
    void mainSearchParts(ActionEvent event) {

    }

    @FXML
    void mainSearchProducts(ActionEvent event) {

    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
